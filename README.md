# lxd_hosting role

Ansible role to setup a physical server and prepare it for DevOps
workloads.

This process cannot be totally automated since it depends on the
hardware and initial OS configuration. (See prerequisities!)

Hosting servers run LXD in a cluster setup to allow for easy migration
of containers between them. It uses ZFS for the main storage.

Storage is abstracted to container workloads into a few storage
classes with pre-defined mount points:
* standard (ZFS)
  * Mounted at `/lxd_hosting/standard`
  * This is the default disk storage
* nearline (ZFS)
  * Mounted at `/lxd_hosting/nearline`
  * This is also disk storage but may be slower than standard. Applications
    that have less frequently used data should store that part on nearline.
* volatile
  * Mounted at `/lxd_hosting/volatile`
  * This is (preferably fast) storage that may not survive a
    reboot. It may e.g. be a RAM-disk.
  * **NOTE:** This is not yet implemented!

Note that two or more of the storage classes can point to the same
actual hardware (and zpool) but all are defined on every hosting
server. The purpose of the abstraction is to allow best utilization of
resources on servers with varying hardware.

Network interfaces use standardized names that tells which network
they are attached to, e.g. 'lan0' and 'dmz'.

## Prerequisites

The following is assumed about the server when this role is used:
- OS: Ubuntu (Server) 18.04 LTS
- There are one or two zpool(s) for container and regular data storage
- ssh and python installed to be able to run Ansible
- The server is attached to the network:
  - IP must be constant, i.e. either static or reserved in DHCP
  - The hostname is registered in DNS
  - The interface that should be used for the LXD overlay network
    must configured (i.e. IP must be set, interface name may change)
  - (The network configuration on the server will be replaced when
    running this role though)

## Requirements

- ansible >= 2.8
- molecule >= 2.19

## Dependencies

This role depends on:
* nic_config
* lxd
* interactive_terminal

## Role Variables

### Network

Network is configured through the role **nic_config**, which allows us
to automate network configuration and to add a layer of abstraction by
naming network interfaces the same on all machines in a
cluster. I.e. all interfaces connected to the internal lan can be
named e.g. 'lan0', all that are connected to DMZ be named 'dmz' and
the ones connected to the LXD Fan Overlay network named 'cluster'.
This way we can move containers between hosts in the cluster and make
them properly use network even when using e.g. macvlan type
interfaces.

Please see documentation for the *nic_config* role!

### Storage

* `lxd_hosting_zfs_filesets`

  A dictionary that tells where standard and nearline storage are
  stored respectively. The values should be ZFS file systems (that
  don't have to exist yet) and may be on different or on the same
  zpool.

  Example:
  ```yaml
  lxd_hosting_zfs_filesets:
    standard: rpool/standard
    nearline: tank/nearline
  ```

  Here standard storage is put on the zpool _rpool_ in a file system
  called _standard_ (that will be created if it does not
  exist). Nearline storage will be stored on another pool, _tank_, on
  the file system _nearline_.

### Hosting

* `lxd_hosting_guests`

  A list of all LXD containers that are maintained by this hosting
  server, i.e. the host-lxd guests. The names in the list are
  inventory host names.


## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: lxd_hosting
```
