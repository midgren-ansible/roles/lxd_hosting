import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_default(host):
    # Test that the defined "storage classes" have been mounted on correct locations
    assert host.mount_point("/lxd_hosting/standard").exists
    assert host.mount_point("/lxd_hosting/nearline").exists


def test_lan0(host):
    # This tests that there is a lan0 interface and that is has IP 192.168.11.1:
    assert host.run_expect([0], "ip addr show dev lan0 | grep 192.168.11.1/24")
    # Check that a static route has been added according to config:
    assert host.run_expect([0], "ip route | grep '192.168.200.0/24 via 192.168.11.1 dev lan0'")
    assert host.run_expect([0], "ip route | grep '192.168.201.0/24 via 192.168.11.1 dev lan0 proto static metric 1200'")
